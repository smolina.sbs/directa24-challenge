package com.directa24.challenge.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.stream.Collectors;

import com.directa24.challenge.enums.AllowedRequestMethodsEnum;

public class HttpUtils {

	private static final int CONNECT_TIMEOUT = 15000;
	private static final int READ_TIMEOUT = 10000;
	private static final String CONTENT_TYPE_KEY = "Content-Type";
	private static final String CONTENT_TYPE_VALUE = "application/json; charset=utf-8";
	private static final String QUERY_STRING_STARTER = "?";
	private static final String QUERY_STRING_SEPARATOR = "&";
	private static final String QUERY_STRING_KEY_ASSIGN_VALUE = "=";
	private static final String JOINER = "\n";

	public static String getHttpRequest(AllowedRequestMethodsEnum method, String endpoint,
			HashMap<String, String> params) throws IOException {
		StringBuilder sb;
		URL url;
		HttpURLConnection conn;

		sb = new StringBuilder();
		sb.append(endpoint);
		sb.append(buildQueryString(params));
		url = new URL(sb.toString());
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(method.name());
		conn.setReadTimeout(READ_TIMEOUT);
		conn.setConnectTimeout(CONNECT_TIMEOUT);
		conn.setRequestProperty(CONTENT_TYPE_KEY, CONTENT_TYPE_VALUE);

		return new BufferedReader(new InputStreamReader(conn.getInputStream())).lines()
				.collect(Collectors.joining(JOINER));
	}

	public static String buildQueryString(HashMap<String, String> params) {
		StringBuilder sb;
		sb = new StringBuilder();
		params.forEach((k, v) -> {
			if (sb.toString().isEmpty()) {
				sb.append(QUERY_STRING_STARTER);
			} else {
				sb.append(QUERY_STRING_SEPARATOR);
			}
			sb.append(k);
			sb.append(QUERY_STRING_KEY_ASSIGN_VALUE);
			sb.append(v);
		});
		return sb.toString();
	}
}
