package com.directa24.challenge;

import com.directa24.challenge.service.UserService;

public class Main {

	public static void main(String[] args) {
		System.out.println("Starting...");
		System.out.println(UserService.getUsersWithSubmissionCountOverThreshold(15).toString());
		System.out.println("...finished");
	}

}
