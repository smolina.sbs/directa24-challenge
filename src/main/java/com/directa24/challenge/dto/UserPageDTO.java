package com.directa24.challenge.dto;

import lombok.ToString;

@ToString(callSuper = true)
public class UserPageDTO extends PageDTO<UserDTO> {

}
