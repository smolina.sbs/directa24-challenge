package com.directa24.challenge.dto;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class UserDTO {
	private Integer id;
	private String username;
	private String about;
	private Integer submitted;
	private Timestamp updated_at;
	private Integer submission_count;
	private Integer comment_count;
	private Integer created_at;
}
