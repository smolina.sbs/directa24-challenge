package com.directa24.challenge.dto;

import java.util.List;

import lombok.Data;

@Data
public class PageDTO<T> {
	private Integer page;
	private Integer per_page;
	private Integer total;
	private Integer total_pages;
	private List<T> data;
}
