package com.directa24.challenge.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.directa24.challenge.dto.UserDTO;
import com.directa24.challenge.dto.UserPageDTO;
import com.directa24.challenge.enums.AllowedRequestMethodsEnum;
import com.directa24.challenge.utils.HttpUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserService {

	private static final int STARTING_PAGE = 1;
	private static final String BE_URL = "https://jsonmock.hackerrank.com/api/article_users";

	public static UserPageDTO getAllByPage(Integer page) {
		UserPageDTO pageDto;
		pageDto = null;
		try {
			ObjectMapper mapper;
			HashMap<String, String> params;
			String responseStringfiedJson;

			mapper = new ObjectMapper();
			params = new HashMap<>();
			params.put("page", page.toString());
			responseStringfiedJson = HttpUtils.getHttpRequest(AllowedRequestMethodsEnum.GET, BE_URL, params);
			pageDto = mapper.readValue(responseStringfiedJson, UserPageDTO.class);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return pageDto;
	};

	public static List<UserDTO> getAll() {
		Integer page;
		List<UserDTO> users;
		UserPageDTO dto;

		page = STARTING_PAGE;
		users = new ArrayList<>();
		do {
			dto = getAllByPage(page);
			page++;
			users.addAll(dto.getData().stream().map(e -> e).collect(Collectors.toList()));
		} while (dto != null && page <= dto.getTotal_pages());
		return users;
	};

	public static List<String> getUsersWithSubmissionCountOverThreshold(Integer threshold) {
		return getAll().stream().filter(e -> e.getSubmission_count() > threshold).map(e -> e.getUsername())
				.collect(Collectors.toList());
	};

}
